#!/bin/bash

## this list is just for tab-completion
## it's not an exhaustive list of platforms available.
AVAILABLE_PLATFORMS="qrb5165 apq8096"

print_usage(){
	echo ""
	echo " Build VOXL docker mavsdk instance to tar and push to modalAI device"
	echo ""
	echo " Usage:"
	echo "  ./build-image.sh {platform}"
	echo ""
	echo " Examples:"
	echo ""
	echo "  ./install_build_deps.sh qrb5165"
	echo "        Build voxl docker mavsdk for qrb5165"
	echo ""
	echo ""
	echo "  ./install_build_deps.sh apq8096"
	echo "        Build voxl docker mavsdk for apq8096"
	echo ""
	echo ""
}

# make sure two arguments were given
if [ "$#" -ne 1 ]; then
	print_usage
	exit 1
fi

## convert arguments to lower case for robustness
PLATFORM=$(echo "$1" | tr '[:upper:]' '[:lower:]')

source third_party/common
echo "Image: "$IMAGE_NAME

# Check if dockerd is running
echo "Checking for docker support on parent computer to build tar voxl mavsdk"
if docker info > /dev/null 2>&1;
then
    DOCKER_RUNNING=true
	echo "-> Docker support is running on target. Continuing to build!"
else
    DOCKER_RUNNING=false
	echo "-> Docker support not detected, please install docker on this device and rerun post installation."
	exit 1
fi

echo "Waiting for voxl device..."
adb wait-for-device

echo "Checking for docker support on modalAI device"
if [ "$PLATFORM" == "apq8096" ]; then
	DOCKER_ACTIVE=$(adb shell systemctl is-active docker-daemon)
else 
	DOCKER_ACTIVE=$(adb shell systemctl is-active docker)
fi

if [[ $DOCKER_ACTIVE == *active* ]]; then
	echo "-> Docker support is running on target."
else
	echo "-> Docker support not detected, running docker support on target..."
	adb shell voxl-configure-docker-support.sh
fi

echo "Building image: $IMAGE_NAME"

docker build . -t $IMAGE_NAME --network=host
