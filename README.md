# voxl-docker-mavsdk

Example of how to use MAVSDK for both Python and C++ in a Docker container running on target.

## Summary

By following this example project, you will:

- Create a docker image on your parent computer with both MAVSDK-Python and MAVSDK as well as be able to run both the takeoff_and_land C++ and python exectubles. This will then get pushed and loaded to the voxl to be deployed and run onboard.

## Prerequisites

- VOXL/VOXL 2 + Parent computer able to connect to VOXL/VOXL2 via adb or ssh
- For installation, the parent computer needs to be connected to the to the voxl. If leveraging ADB, a serial connection must be made to the voxl/voxl2, or if using ssh instead, the voxl must have an internet connection.

## Building from source

To build the docker image with MAVSDK and the example program:

- Ensure you have docker installed on the parent computer in which we will be building the container
- If not installed, run `./install_build_deps.sh` which will install docker on device (tested on both Ubuntu/MacOS)
- Once docker has been installed, validate it via `docker info` - this will output information associated to the docker daemon
- Connect to the VOXL2 via a usb connection - the build script will check and ensure docker is installed onboard the voxl
- run the build script:

```bash
./build.sh
```

Note: this is a LONG process, it takes **roughly 20 minutes**:

```bash
Step 20 : COPY takeoff_and_land2.py .
 ---> 2f5a4181cbc8
Removing intermediate container 7baafa6060a4
Step 21 : CMD /bin/bash
 ---> Running in ab8fad26d032
 ---> 98d4b52a5860
Removing intermediate container ab8fad26d032
Successfully built 98d4b52a5860bf0226e7c
Successfully built ec9462df4305
```

Once this is done running, the user can then run the `make_package.sh` script which will package the docker image into a tar file to be pushed to the voxl.

Once the image has been made into a tar.gz file, proceed to deploy the file and unpackage/load the docker tar via the `deploy_to_voxl.sh` script - if leveraging serial adb, just run the `deploy_to_voxl.sh` bare, but if leveraging ssh, proceed to pass in `ssh` after the executable.

Once this is done running, the user then can `adb shell` or `ssh` into the voxl and run the following command to run/be placed into the docker instance:

```
docker run -it --rm --privileged --net=host --ipc=host voxl-mavsdk:v1.2
```

## Execute the Example Program

### Overview

After the docker image is build following the steps above, adb shell into the voxl2 and run the scripts that are placed in the /home directory. Both takeoff_and_land (CPP) and takeoff_and_land.py (Python) are there and ready to be executed at your convenience.

### Usage

#### Warning

This example instructs the vehicle to takeoff to an altitude of roughly 2.5m, hold for several seconds, and then land.

- Run the example `./voxl2_takeoff_and_land.py`: this will takeoff the drone and land after a few seconds.

- To run the CPP instance, run the same thing with just `./takeoff_and_land`

Here's output from a test flight from the previous command:

```bash
root@apq8096:/home/MAVSDK-Python/examples# python3 takeoff_and_land2.py udp://:14551
Waiting for mavsdk_server to be ready...
Connected to mavsdk_server!
Waiting for drone to connect...
Waiting for drone to have a global position estimate...
Global position estimate ok
-- Arming
[05:03:02|Debug] MAVLink: info: ARMED by Arm/Disarm component command (system_impl.cpp:257)
-- Taking off
[05:03:02|Debug] MAVLink: info: [logger] /fs/microsd/log/2020-07-10/17_03_02.ulg (system_impl.cpp:257)
[05:03:02|Debug] MAVLink: info: Using minimum takeoff altitude: 2.50 m (system_impl.cpp:257)
[05:03:04|Debug] MAVLink: info: Takeoff detected (system_impl.cpp:257)
-- Landing
[05:03:07|Debug] MAVLink: info: Landing at current position (system_impl.cpp:257)
root@apq8096:/home/MAVSDK-Python/examples# [05:03:14|Debug] MAVLink: info: Landing detected (system_impl.cpp:257)
[05:03:16|Debug] MAVLink: info: DISARMED by Auto disarm initiated (system_impl.cpp:257)
```

