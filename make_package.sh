#!/bin/bash
################################################################################
# Copyright (c) 2022 ModalAI, Inc. All rights reserved.
#
# Semi-universal script for making a deb and ipk package. This is shared
# between the vast majority of VOXL-SDK packages
#
# Add the 'timestamp' argument to add a date-timestamp suffix to the deb package
# version. This is used by CI for making nightly and development builds.
#
# author: james@modalai.com
################################################################################
source third_party/common

print_usage(){
	echo ""
	echo " Package the current docker instance into tar file."
	echo " You must run build.sh first to build the initial docker container"
	echo ""
	echo " Usage:"
	echo "  ./make_package.sh"
	echo ""
}

echo "Saving image to tar file!"

docker save -o $IMAGE_NAME.tar.gz $IMAGE_NAME

echo "Done!"