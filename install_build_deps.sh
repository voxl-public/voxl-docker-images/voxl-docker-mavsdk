#!/bin/bash
################################################################################
# Copyright (c) 2022 ModalAI, Inc. All rights reserved.
################################################################################

# Script to install build dependencies in voxl-cross docker image

# list all your dependencies here. Note for packages that have AMD64 equivalents
# in the ubuntu repositories you should specify the arm64 architecture to make
# sure the correct one is installed in voxl-cross.
DEPS_QRB5165=( "
" )

DEPS_APQ8096=( "
" )


## this list is just for tab-completion
## it's not an exhaustive list of platforms available.
AVAILABLE_PLATFORMS="qrb5165 apq8096"


print_usage(){
    echo ""
    echo " Install build dependencies from a specified repository."
    echo ""
    echo " Usage:"
    echo "  ./install_build_deps.sh"
    echo ""
    echo " Examples:"
    echo ""
    echo "  ./install_build_deps.sh"
    echo "        Install from apq8096 development repo."
    echo ""
    echo ""
    echo " These examples are not an exhaustive list."
    echo " Any platform and section in this deb repo can be used:"
    echo "     http://voxl-packages.modalai.com/dists/"
    echo ""
}

# install deb packages with apt
./third_party/get-docker.sh

echo ""
echo "Done installing dependencies"
echo ""
exit 0
