#!/usr/bin/env python3

import csv
import os
import asyncio
from datetime import datetime
from mavsdk import System


async def run():
    mission_succeed = False

    drone = System()
    await drone.connect(system_address="udp://:14551")

    print("Waiting for drone to connect...")
    async for state in drone.core.connection_state():
        if state.is_connected:
            print(f"Drone connected!")
            break

    print("Waiting for drone to have a global position estimate...")
    async for health in drone.telemetry.health():
        if health.is_global_position_ok:
            print("Global position estimate ok")
            break

    print("-- Arming")
    await drone.action.arm()

    print("-- Taking off")
    await drone.action.takeoff()

    await asyncio.sleep(15)

    print("-- Landing")
    await drone.action.land()
    mission_succeed = True

    log_mission(mission_succeed)

def log_mission(mission_succeed):
    csv_file_path = '/app/output/hitl-mission-logs.csv'
    csv_dir = os.path.dirname(csv_file_path)
    
    if not os.path.exists(csv_dir):
        os.makedirs(csv_dir)
    
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    with open(csv_file_path, mode='a', newline='') as file:
        writer = csv.writer(file)
        if mission_succeed:
            writer.writerow([timestamp, "Mission succeeded."])
        else:
            writer.writerow([timestamp, "Misison failed."])

if __name__ == "__main__":
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(run())
    except Exception as e:
        print(f"Error occurred: {e}")
        log_mission(False)
