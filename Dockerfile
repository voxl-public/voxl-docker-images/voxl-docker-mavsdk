FROM arm64v8/ubuntu:focal

WORKDIR /home

RUN apt-get update
RUN apt-get install -y apt-transport-https ca-certificates gnupg software-properties-common wget
RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | apt-key add -
RUN apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main'
RUN apt-get update
RUN apt-get install -y cmake build-essential colordiff git doxygen -y
RUN apt-get install -y python3 python3-pip -y
RUN apt-get install -y git libxml2-dev libxslt-dev curl

RUN git clone https://github.com/mavlink/MAVSDK.git

WORKDIR /home/MAVSDK
RUN git checkout main
RUN git submodule update --init --recursive

RUN cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_MAVSDK_SERVER=ON -DBUILD_SHARED_LIBS=ON -Bbuild/default -H.
RUN cmake --build build/default --target install -- -j 4
RUN ldconfig

WORKDIR /home/MAVSDK/examples/takeoff_and_land
RUN cmake .
RUN make

WORKDIR /home

RUN python3 -m pip install -U pip
RUN python3 -m pip install asyncio
RUN python3 -m pip install mavsdk

WORKDIR /home
COPY third_party/voxl2_takeoff_land.py /home/voxl2_takeoff_land.py

CMD ["/bin/bash"]
