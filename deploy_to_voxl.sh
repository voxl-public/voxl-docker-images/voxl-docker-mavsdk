#!/bin/bash
################################################################################
# Copyright (c) 2022 ModalAI, Inc. All rights reserved.
#
# Requires the package to be built and an adb connection.
################################################################################
set -e

SSH_PWD="oelinux123"
source third_party/common


print_usage(){
	echo ""
	echo " Deploy the docker tar file package to VOXL over adb or ssh"
	echo " You must run ./make_package.sh first to build the package."
	echo ""
	echo " Usage to push over adb:"
	echo "  ./deploy_to_voxl.sh"
	echo "  ./deploy_to_voxl.sh adb"
	echo ""
	echo " Usage to push over ssh:"
	echo "   if VOXL_IP env variable is set:"
	echo "  ./deploy_to_voxl.sh ssh"
	echo ""
	echo "   manually specifying a full IP address:"
	echo "  ./deploy_to_voxl.sh ssh 192.168.1.123"
	echo ""
	echo "   manually specifying last block of the IP"
	echo "  ./deploy_to_voxl.sh ssh 123"
	echo ""
}

#check adb (default) or ssh or print help text
if [ "$#" -lt 1 ]; then
	#no options = default adb
	DEPLOY_MODE="ADB"
else
	#Parse arg
	case $1 in

		"adb")
			DEPLOY_MODE="adb"
			;;

		"ssh")
			DEPLOY_MODE="ssh"
			#check command line for push ip
			if [ "$#" -gt 1 ]; then
				if  echo $2 | grep -xq "[0-9]*" ; then
					#echo "Number"
					SEND_IP=192.168.1.$2
				elif  echo $2 | grep -xq "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" ; then
					#echo "Full IP"
					SEND_IP=$2
				else
					echo "Invalid IP address: $2"
					exit 1
				fi
			elif ! [ -z ${VOXL_IP+x} ]; then
				SEND_IP="${VOXL_IP}"
			else
				echo "Did not find a VOXL_IP env variable,"
				echo ""
				echo "If you would like to push over ssh automatically,"
				echo "please export VOXL_IP in your bashrc"
				echo ""
				read -p "Please enter an IP to push to:" SEND_IP
			fi
			;;

		"h"|"-h"|"help"|"--help")
			print_usage
			exit 0
			;;

		*)
			print_usage
			exit 1
			;;
	esac
fi

if [ "$DEPLOY_MODE" == "ssh" ]; then
	if ! command -v sshpass &> /dev/null ;then
		echo ""
		echo "You do not have sshpass installed"
		echo "Please install sshpass to use the install via ssh feature"
		echo ""
		exit 1
	fi

	echo "searching for ssh device"
	until ping -c1 $SEND_IP &>/dev/null; do :; done

	if ping -c1 $SEND_IP &>/dev/null; then
        sshpass -p oelinux123 ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@$SEND_IP "mkdir -p $TARGET_DIR" 2>/dev/null
        echo "Pushing over mavsdk docker tar file"
        sshpass -p oelinux123 scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $(pwd)/$IMAGE_NAME.tar.gz root@$SEND_IP:$TARGET_DIR &>/dev/null
        echo "Loading docker tar file on voxl"
		sshpass -p oelinux123 ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@$SEND_IP "docker load -i $TARGET_DIR/$IMAGE_NAME.tar.gz" 2>/dev/null
	else
		echo "Unable to push tar and load docker instance onboard"
		exit 1
	fi

else
	if ! command -v adb &> /dev/null ;then
		echo ""
		echo "You do not have adb installed"
		echo "Please install adb to use the install via adb feature"
		echo ""
		exit 1
	fi

	echo "searching for ADB device"
	adb wait-for-device

	if adb wait-for-device; then
        adb shell mkdir -p $TARGET_DIR
        adb push $IMAGE_NAME.tar.gz $TARGET_DIR
        adb shell docker load -i $TARGET_DIR/$IMAGE_NAME.tar.gz
	else
		echo "ERROR neither dpkg nor opkg found on VOXL"
		exit 1
	fi
fi

echo "DONE"
